# b2test
b2-test task

CI/CD wordpress(nginx + php-fpm)
Dockerfile build + deploy

applied changes:
php_value upload_max_filesize 128M
php_value post_max_size 128M

sudo ufw status
sudo ufw allow 22
sudo ufw allow 80
sudo ufw allow 443
sudo ufw default allow outgoing
sudo ufw default deny incoming

GitLab-CI (
    docker build from Dockerfile -> docker push
    docker pull on Host -> docker run
    )
Repository
    {:master} = dev for ALL
    {:stage} =^ production

Developer cannot merge to {:stage} and cannot break pipeline.
