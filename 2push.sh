#!/bin/bash

cd ~/b2test/
DATE=$( date "+%Y-%m-%d %H:%M")
file=".git/new-push.txt"
git status > $file
git add .

if [ -z "$1" ]; then  #if not yet cmd parameters
    git commit -m"From $HOSTNAME at ${DATE}"
else
    #echo "$1";
    git commit -m"$1";
fi
git push origin master
