<?php
/**
 * Основные параметры WordPress.
 
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'b2test' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'b2test' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'b2test' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

//https://api.wordpress.org/secret-key/1.1/salt/
define('AUTH_KEY',         '1|!b_.Fv}j^+JmR60sG-,oK-)<56_zq}-4~K`p`e-L_Z: !-cedjW!1J0m%u{#RX');
define('SECURE_AUTH_KEY',  '2IePo(^9cmpTG$C!.^;Ao-i]v}KsTdV~1(o|u#+E(z[V%KHZ3lJ/Xo`#4VY7]3U>');
define('LOGGED_IN_KEY',    '3QT/Z*a@/ ?FZ%t~&coA-FDqa:V~E*6W7W;a@t~W?rR5 L,Vy)Lk~t##Mu+-S9fN');
define('NONCE_KEY',        '4aZvk.1i.I+*mV}|yApMI=;b1(^jB*vI-4P3#n~V~N^RrhK@$1mz|M%^_L/pYvP7');
define('AUTH_SALT',        '5&p^` -%IE;|rCsw,{U}J]f(xG^!zyNRi0%+hS<TS*EYkiw$+XB6<btUz%~aU[Wd');
define('SECURE_AUTH_SALT', '6yg!fycb(t#9[(#cHSB3-s+O;q.p3W*nKRNol/F?qzyWA;.=a*X>*DX?_$iw!A1d');
define('LOGGED_IN_SALT',   '7=F^~4NG`by1qQ^[<}l,B/GA(VTB*[PLm^l-:*SP]$.`X#ypX>{zx1U+{}O0f>&r');
define('NONCE_SALT',       '8CLr#1UadjA/Il`[J~oP*@A]F.JCP+i,_=c+k[s`Rr-o73SJ9OGaZ@6pJa?f<t%/');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
